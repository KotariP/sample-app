# SampleApp CICD Deployment Approach 2

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.

Steps:

1. Install Angular CLI and Node JS.
2. Go to the project directory
3. Open commmand prompt and run the command `npm install` to install dependencies on angular side
4. Go to server folder and run `npm install` to install dependencies on server side.
5. Now go back to the project directory and run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
6. Copy the `dist` folder generated in the previous step into the server folder.
7. To start the server, run the command `node ./bin/www` from server folder.
